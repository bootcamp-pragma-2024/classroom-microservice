package com.pragma.classroom.adapters.driven.jpa.mysql.repository;

import com.pragma.classroom.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TechnologyRepository extends JpaRepository<TechnologyEntity, Long> {

    Optional<TechnologyEntity> findByName(String name);

}
