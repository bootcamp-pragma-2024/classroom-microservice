package com.pragma.classroom.adapters.driven.jpa.mysql.mapper;

import com.pragma.classroom.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import com.pragma.classroom.domain.model.TechnologyModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TechnologyEntityMapper {

    TechnologyModel toModel(TechnologyEntity technologyEntity);
    TechnologyEntity toEntity(TechnologyModel technologyModel);

}
