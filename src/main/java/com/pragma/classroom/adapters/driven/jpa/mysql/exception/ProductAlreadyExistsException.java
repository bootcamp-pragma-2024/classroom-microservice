package com.pragma.classroom.adapters.driven.jpa.mysql.exception;

public class ProductAlreadyExistsException extends RuntimeException {
    public ProductAlreadyExistsException() {
        super();
    }
}
