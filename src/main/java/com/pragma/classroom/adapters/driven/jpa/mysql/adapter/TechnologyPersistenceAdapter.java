package com.pragma.classroom.adapters.driven.jpa.mysql.adapter;

import com.pragma.classroom.adapters.driven.jpa.mysql.mapper.TechnologyEntityMapper;
import com.pragma.classroom.adapters.driven.jpa.mysql.repository.TechnologyRepository;
import com.pragma.classroom.domain.model.TechnologyModel;
import com.pragma.classroom.domain.spi.TechnologyPersistencePort;
import org.springframework.stereotype.Component;

import java.util.Optional;

public class TechnologyPersistenceAdapter implements TechnologyPersistencePort {

    private final TechnologyRepository technologyRepository;
    private final TechnologyEntityMapper technologyEntityMapper;

    public TechnologyPersistenceAdapter(TechnologyRepository technologyRepository,
                                        TechnologyEntityMapper technologyEntityMapper) {
        this.technologyRepository = technologyRepository;
        this.technologyEntityMapper = technologyEntityMapper;
    }

    @Override
    public TechnologyModel save(TechnologyModel technologyModel) {
        return technologyEntityMapper.toModel(
                technologyRepository.save(
                        technologyEntityMapper.toEntity(technologyModel)
                )
        );
    }

    @Override
    public Optional<TechnologyModel> findByName(String name) {
        return Optional.ofNullable(
                technologyEntityMapper.toModel(
                        technologyRepository.findByName(name).orElse(null)
                )
        );
    }
}
