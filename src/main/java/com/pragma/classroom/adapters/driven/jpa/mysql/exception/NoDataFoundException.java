package com.pragma.classroom.adapters.driven.jpa.mysql.exception;

public class NoDataFoundException extends RuntimeException {
    public NoDataFoundException() {
        super();
    }
}
