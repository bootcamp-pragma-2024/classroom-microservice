package com.pragma.classroom.adapters.driving.http.mapper;

import com.pragma.classroom.adapters.driving.http.dto.request.TechnologyRequest;
import com.pragma.classroom.domain.model.TechnologyModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TechnologyRequestMapper {

    TechnologyModel toModel(TechnologyRequest technologyRequest);

}
