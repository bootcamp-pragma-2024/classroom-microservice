package com.pragma.classroom.adapters.driving.http.controller;

import com.pragma.classroom.adapters.driving.http.dto.request.TechnologyRequest;
import com.pragma.classroom.adapters.driving.http.mapper.TechnologyRequestMapper;
import com.pragma.classroom.domain.api.TechnologyServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/technology")
public class TechnologyController {

    private final TechnologyServicePort technologyService;
    private final TechnologyRequestMapper technologyRequestMapper;

    @PostMapping("/")
    public ResponseEntity<Map<String, Object>> save(@RequestBody TechnologyRequest technologyRequest) {
        Map<String, Object> response = new HashMap<>();
        technologyService.save(technologyRequestMapper.toModel(technologyRequest));
        response.put("success", true);
        response.put("message", "Se guarda correctamente la tecnologia");

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

}