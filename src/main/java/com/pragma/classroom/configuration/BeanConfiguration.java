package com.pragma.classroom.configuration;

import com.pragma.classroom.adapters.driven.jpa.mysql.adapter.ProductAdapter;
import com.pragma.classroom.adapters.driven.jpa.mysql.adapter.SupplierAdapter;
import com.pragma.classroom.adapters.driven.jpa.mysql.adapter.TechnologyPersistenceAdapter;
import com.pragma.classroom.adapters.driven.jpa.mysql.mapper.IProductEntityMapper;
import com.pragma.classroom.adapters.driven.jpa.mysql.mapper.ISupplierEntityMapper;
import com.pragma.classroom.adapters.driven.jpa.mysql.mapper.TechnologyEntityMapper;
import com.pragma.classroom.adapters.driven.jpa.mysql.repository.IProductRepository;
import com.pragma.classroom.adapters.driven.jpa.mysql.repository.ISupplierRepository;
import com.pragma.classroom.adapters.driven.jpa.mysql.repository.TechnologyRepository;
import com.pragma.classroom.domain.api.IProductServicePort;
import com.pragma.classroom.domain.api.ISupplierServicePort;
import com.pragma.classroom.domain.api.TechnologyServicePort;
import com.pragma.classroom.domain.spi.TechnologyPersistencePort;
import com.pragma.classroom.domain.usecase.ProductUseCase;
import com.pragma.classroom.domain.usecase.SupplierUseCase;
import com.pragma.classroom.domain.spi.IProductPersistencePort;
import com.pragma.classroom.domain.spi.ISupplierPersistencePort;
import com.pragma.classroom.domain.usecase.TechnologyUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
    private final IProductRepository productRepository;
    private final IProductEntityMapper productEntityMapper;
    private final ISupplierRepository supplierRepository;
    private final ISupplierEntityMapper supplierEntityMapper;
    private final TechnologyRepository technologyRepository;
    private final TechnologyEntityMapper technologyEntityMapper;

    @Bean
    public IProductPersistencePort productPersistencePort() {
        return new ProductAdapter(productRepository, productEntityMapper, supplierRepository, supplierEntityMapper);
    }
    @Bean
    public IProductServicePort productServicePort() {
        return new ProductUseCase(productPersistencePort());
    }
    @Bean
    public ISupplierPersistencePort supplierPersistencePort() {
        return new SupplierAdapter(supplierRepository, supplierEntityMapper);
    }
    @Bean
    public ISupplierServicePort supplierServicePort() {
        return new SupplierUseCase(supplierPersistencePort());
    }

    @Bean
    public TechnologyPersistencePort technologyPersistencePort() {
        return new TechnologyPersistenceAdapter(technologyRepository, technologyEntityMapper);
    }

    @Bean
    public TechnologyServicePort technologyServicePort() {
        return new TechnologyUseCase(technologyPersistencePort());
    }

}
