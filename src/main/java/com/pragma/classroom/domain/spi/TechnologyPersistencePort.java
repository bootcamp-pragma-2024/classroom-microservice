package com.pragma.classroom.domain.spi;

import com.pragma.classroom.domain.model.TechnologyModel;

import java.util.Optional;

public interface TechnologyPersistencePort {

    TechnologyModel save(TechnologyModel technologyModel);
    Optional<TechnologyModel> findByName(String name);

}