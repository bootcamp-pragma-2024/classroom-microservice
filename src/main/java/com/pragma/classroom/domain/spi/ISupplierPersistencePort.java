package com.pragma.classroom.domain.spi;

import com.pragma.classroom.domain.model.Supplier;

import java.util.List;

public interface ISupplierPersistencePort {
    void addSupplier(Supplier supplier);
    List<Supplier> getAllSuppliers();
}
