package com.pragma.classroom.domain.usecase;

import com.pragma.classroom.domain.api.ISupplierServicePort;
import com.pragma.classroom.domain.model.Supplier;
import com.pragma.classroom.domain.spi.ISupplierPersistencePort;

import java.util.List;

public class SupplierUseCase implements ISupplierServicePort {
    private final ISupplierPersistencePort supplierPersistencePort;

    public SupplierUseCase(ISupplierPersistencePort supplierPersistencePort) {
        this.supplierPersistencePort = supplierPersistencePort;
    }

    @Override
    public void addSupplier(Supplier supplier) {
        supplierPersistencePort.addSupplier(supplier);
    }

    @Override
    public List<Supplier> getAllSuppliers() {
        return supplierPersistencePort.getAllSuppliers();
    }
}
