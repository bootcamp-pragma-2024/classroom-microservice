package com.pragma.classroom.domain.usecase;

import com.pragma.classroom.domain.api.TechnologyServicePort;
import com.pragma.classroom.domain.exception.EmptyFieldException;
import com.pragma.classroom.domain.exception.TechnologyAlreadyExistsException;
import com.pragma.classroom.domain.model.TechnologyModel;
import com.pragma.classroom.domain.spi.TechnologyPersistencePort;

public class TechnologyUseCase implements TechnologyServicePort {

    private final TechnologyPersistencePort technologyPersistence;

    public TechnologyUseCase(TechnologyPersistencePort technologyPersistence) {
        this.technologyPersistence = technologyPersistence;
    }

    @Override
    public void save(TechnologyModel technologyModel) {
        if (technologyPersistence.findByName(technologyModel.getName()).isPresent()) {
            throw new TechnologyAlreadyExistsException();
        }

        if (technologyModel.getDescription().isBlank()) {
            throw new EmptyFieldException("Description cannot be null");
        }

        technologyPersistence.save(technologyModel);
    }

}