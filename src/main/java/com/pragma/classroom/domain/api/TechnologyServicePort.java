package com.pragma.classroom.domain.api;

import com.pragma.classroom.domain.model.TechnologyModel;

public interface TechnologyServicePort {

    void save(TechnologyModel technologyModel);

}
