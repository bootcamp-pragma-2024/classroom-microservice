package com.pragma.classroom.domain.api;

import com.pragma.classroom.domain.model.Supplier;

import java.util.List;

public interface ISupplierServicePort {
    void addSupplier(Supplier supplier);
    List<Supplier> getAllSuppliers();
}
