package com.pragma.classroom.domain.factory;

import com.pragma.classroom.domain.model.TechnologyModel;

public class TechnologyBuilder {

    private Long id;
    private String name;
    private String description;

    public TechnologyBuilder() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public TechnologyBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public TechnologyBuilder name(String name) {
        this.name = name;
        return this;
    }

    public TechnologyBuilder description(String description) {
        this.description = description;
        return this;
    }

    public static TechnologyBuilder builder() {
        return new TechnologyBuilder();
    }


    public TechnologyModel build() {
        TechnologyModel technology = new TechnologyModel();
        technology.setId(getId());
        technology.setName(getName());
        technology.setDescription(getDescription());

        return technology;
    }
}
