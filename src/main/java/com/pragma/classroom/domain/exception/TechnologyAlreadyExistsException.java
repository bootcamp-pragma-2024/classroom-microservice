package com.pragma.classroom.domain.exception;

public class TechnologyAlreadyExistsException extends RuntimeException {

    public TechnologyAlreadyExistsException() {
        super();
    }

}
